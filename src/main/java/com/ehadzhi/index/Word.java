package com.ehadzhi.index;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * Created by ehadzhi on 5/24/17.
 */
public class Word {
    private String value;
    private String fileName;
    private int row;

    public Word(String value, String fileName, int row) {
        this.value = value;
        this.fileName = fileName;
        this.row = row;
    }

    public String getValue() {
        return value;
    }

    public String getFileName() {
        return fileName;
    }

    public int getRow() {
        return row;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}

package com.ehadzhi.index;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by ehadzhi on 5/24/17.
 */
public class FileUtil {
    public static final String NON_WORD_CHARACTERS = "[\\W]";

    public static Stream<Path> getFilesDeep(String rootPath) {
        try {
            return Files.walk(Paths.get(rootPath))
                    .filter(path -> path.toFile().isFile());
        } catch (IOException e) {
            System.out.println("Something went wrong fetching files from path=" + rootPath + " " + e.getMessage());
        }
        return Stream.empty();
    }

    public static Stream<Word> splitFileIntoWords(Path file) {
        try {
            List<String> lines = Files.lines(file).map(String::toLowerCase).collect(Collectors.toList());

            return intStream(lines.size())
                    .flatMap(index -> {
                        List<String> words = Arrays.asList(lines.get(index).split(NON_WORD_CHARACTERS));
                        return words.stream()
                                .filter(word -> !word.equals(""))
                                .map(word -> new Word(word, file.getFileName().toString(), index));
                    });
        } catch (IOException e) {
            System.out.println("Something went wrong splitting file=" + file + " " + e.getMessage());
        }
        return Stream.empty();
    }

    private static Stream<Integer> intStream(int size) {
        return IntStream.range(0, size).mapToObj(i -> i);
    }
}

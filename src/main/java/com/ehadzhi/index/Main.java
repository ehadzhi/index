package com.ehadzhi.index;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<String> arguments = Arrays.asList(args);
        String path = "./indextestfolder";
        int summary = 5;
        String query = "Lorem";
        try {
            for (int i = 0; i < args.length; i++) {
                if (arguments.get(i).equals("--index")) {
                    path = args[i + 1];
                }
                if (arguments.get(i).equals("--summary")) {
                    summary = Integer.parseInt(args[i + 1]);
                }
                if (arguments.get(i).equals("--search")) {
                    query = args[i + 1];
                }
            }
        } catch (Exception e) {
            System.out.println("Something went wrong obtaining arguments= " + args + " " + e.getMessage());
        }

        try {
            Index index = new Index(path);
            System.out.println("--SUMMARY--");
            index.summary(summary).stream().forEach(System.out::println);
            System.out.println("--SEARCH--");
            index.search(query).stream().forEach(System.out::println);
        } catch (Exception e) {
            System.out.println("Something creating index args=" + Arrays.toString(args) + " " + e.getMessage());
        }
    }
}

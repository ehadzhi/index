package com.ehadzhi.index;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by ehadzhi on 5/24/17.
 */
public class Index {
    private Map<String, Map<String, List<Integer>>> indexedData;
    private String rootPath;
    private ObjectMapper mapper = new ObjectMapper();

    public Index(String rootPath) {
        this.rootPath = rootPath;
        this.indexedData = FileUtil.getFilesDeep(rootPath)
                .flatMap(FileUtil::splitFileIntoWords)
                .collect(Collectors.groupingBy(Word::getValue))
                .entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> convertToIndexFormat(entry.getValue())));
        this.persistIndex();
    }

    private Map<String, List<Integer>> convertToIndexFormat(List<Word> words) {
        Map<String, List<Integer>> filesToRows = new HashMap<>();
        words.forEach(word -> {
            List<Integer> rows = filesToRows.getOrDefault(word.getFileName(), new ArrayList<>());
            rows.add(word.getRow());
            filesToRows.put(word.getFileName(), rows);
        });

        return filesToRows;
    }

    private void persistIndex() {
        Path file = Paths.get(rootPath.concat("/../index.txt"));
        try {
            Files.write(file, Arrays.asList(mapper.writeValueAsString(indexedData)));
        } catch (IOException e) {
            System.out.println("Something went wrong persisting index to file=" + file + " " + e.getMessage());
        }
    }

    public Set<String> search(String query) {
        query = query.toLowerCase();
        List<String> words = Arrays.asList(query.split(FileUtil.NON_WORD_CHARACTERS));

        return words.stream()
                .map(word -> indexedData.getOrDefault(word, new HashMap<>()).keySet())
                .reduce(this::intersect)
                .orElse(Collections.emptySet());
    }

    private Set<String> intersect(Set<String> set1, Set<String> set2) {
        Set<String> intersected = new HashSet<>(set1);
        intersected.retainAll(set2);
        return intersected;
    }

    public List<Map.Entry<String, Map<String, List<Integer>>>> summary(int numWords) {
        return indexedData.entrySet().stream()
                .sorted(Comparator.comparingInt(word -> -numEncounters(word.getKey())))
                .limit(numWords).collect(Collectors.toList());
    }

    public int numEncounters(String word) {
        return indexedData.getOrDefault(word, new HashMap<>())
                .values().stream()
                .mapToInt(list -> list.size())
                .reduce(0, (row1, row2) -> row1 + row2);
    }
}

package com.ehadzhi.index;


import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class IndexTest {

    @Test
    public void testSearch() {
        Index index = new Index("./indextestfolder");
        assertTrue(index.search("testwordone testwordtwo").contains("one"));
    }

    @Test
    public void testSummary() {
        Index index = new Index("./indextestfolder");
        assertEquals("summarytestword", index.summary(5).get(4).getKey());
    }

    @Test
    public void testNumEncounters() {
        Index index = new Index("./indextestfolder");
        assertEquals(2, index.numEncounters("numencountersterstword"));
        assertEquals(0, index.numEncounters("somemadeupwordnotintext"));
    }


}

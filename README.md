The program creates index based on all the text files in a folder and it's subfolders.
Upon start the program traverses all files in the folder and subfolder,
after that from those files all the words are extracted, each word has a origin file and 
and origin row in the file. With that information and index is created with this structure
{"word1":{"file1":[row, row2, ...], ...}, ...}

After creation the index is persisted in a file named index.txt at the same level as the
 given folder.
 
Example usage:

mvn package
java -jar ./target/index-1.0-SNAPSHOT.jar  --index "./indextestfolder" --summary "2" --search "lorem ipsum"